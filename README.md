# Constellation Detection

### About :

This project is focused on helping budding astronomers and space enthusiasts to detect the constellations easily. We are acheiving this by building a web application wherein the user uploads the image of the sky and the result is a predicted constellation in the image along with data about that constellation.

As of now we are building a POC for that we took five constellations : Andromeda, Gemini, Orion, Pisces, Ursa Major
### File Structure :

~/:
- Image Processing
    - Star coordinates files
    - Ratios of the triangles formed by them 
    - Images of constellation
    - Test image
    - starextract.py(For extraction of star coordinates from the image)
- CodeWorking.docx(Description of how code works)
- Detection.py (Python file for detection of constellation)

