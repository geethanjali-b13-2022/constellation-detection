from math import sqrt
import itertools

def triangleMap(starset):                   #Drawing triangles from the coordinates and converting their sides to ratios
	triangleSides = [];
	trianglesSet = permute3(starset)
	for triangle in trianglesSet:
		triangleSides.append(convertToRatio(calculateSides(triangle)))
	return triangleSides

def permute3(starset):                      #Drawing triangles from the coordinates
	trianglesSet = [];
	n = len(starset);
	for i in range(0,n-2):
		for j in range(i+1,n-1):
			for k in range(j+1,n):
				trianglesSet.append([starset[i],starset[j],starset[k]]);
	return trianglesSet;

def calculateSides(triangle):               #Calculating sides length
	sides = [];
	sides.append(dist2Points([triangle[0],triangle[1]]));
	sides.append(dist2Points([triangle[1],triangle[2]]));
	sides.append(dist2Points([triangle[2],triangle[0]]));
	return sides;

def dist2Points(points):                    #Distance formula for calculating side length
	return sqrt((points[0][0] - points[1][0])**2  + (points[0][1] - points[1][1])**2 );

def convertToRatio(vector):                 #Converting Lengths to ratios
	vector.sort();
	vectorRatio = [l/vector[0] for l in vector];
	return vectorRatio;

def constellationMatch( ConstRatioA, ConstRatioB):
	
	matchScore = 0.0

	SetA = ConstRatioA	
	SetB = ConstRatioB
	Num	= len(SetA)

	for A in SetA:
		for B in SetB:
			if (matches(A, B)):
				matchScore += 1.0

	return matchScore

def matches(A,B):
	matchTolerance = 0.000001
	rms = sqrt( (A[1]-B[1])**2 + (A[2]-B[2])**2 )/2
	if (rms < matchTolerance):
		print (rms)
		return True
	else:
		return False

with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/test.star' ,"r") as f:
	lines = f.readlines()
stars = [ [int(l[:-1].split(',')[0]), int(l[:-1].split(',')[1])] for l in lines ]       #Reading each coordinate in the test image

ConstellationRatios = triangleMap(stars)    # Making triangle maps of test image and converting into ratios

#Opening test image ratios
with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/test.txt' ,"w") as fStarOut:
	for ratio in ConstellationRatios:
		fStarOut.write("{}\n".format(ratio))

# print(ConstellationRatios

def ComparisonFiles(num):
	
	if (num==1):
		with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/Andromeda5.txt' , "r") as f_andromeda:
			lines1 = f_andromeda.readlines()
		for l in lines1:
			AndromedaRatios = l
			return (AndromedaRatios)
	elif (num==2):
		with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/Gemini1.txt' , "r") as f_gemini:
			lines2 = f_gemini.readlines()
		for l in lines2:
			GeminiRatios = l
			return (GeminiRatios)
	elif (num==3):
		with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/Orion6.txt' , "r") as f_orion:
			lines3 = f_orion.readlines()
		for l in lines3:
			OrionRatios = l
			return (OrionRatios)
	elif (num==4):
		with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/Pisces12.txt' , "r") as f_pisces:
			lines4 = f_pisces.readlines()
		for l in lines4:
			PiscesRatios = l
			return (PiscesRatios)
	elif (num==5):
		with open('/home/srilekha/Desktop/Workspace/Major Project/ConstellationDetection/Image Processing/UrsaMajor26.txt' , "r") as f_ursa:
			lines5 = f_ursa.readlines()
		for l in lines5:
			UrsaRatios = l
			return (UrsaRatios)
			
for i in range(5):
	count=1
	Score = constellationMatch(ConstellationRatios,ComparisonFiles(count))
	print(Score)
	count+=1